FROM python:3.7-slim
ENV PYTHONUNBUFFERED 1

# Install packages: postgres, python, git etc
RUN apt-get update && \
    mkdir -p /usr/share/man/man1 && \
    mkdir -p /usr/share/man/man7  && \
    apt-get install -y \
        build-essential \
        gcc \
        gettext \
        postgresql-client \
        python3-dev \
        libpq-dev \
        netcat \
        curl \
        libssl-dev \
        gnupg \
        git \
        binutils \
        libproj-dev \
        gdal-bin \
    --no-install-recommends && rm -rf /var/lib/apt/lists/*

# Install node / npm
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get install -y nodejs
RUN npm install -g npm n
