Kingston Labs Docker Base PostGIS Image
=======================================

This is a base ubuntu image that includes Python 3.7, Django requirements, Node and PostGIS.